const express = require("express");
const router = express.Router();

const checkAuth = require("../middleware/check-auth");

const radiusControl = require("../controllers/radius");

router.get("/credentials", checkAuth, radiusControl.radius_get_credentials);

module.exports = router;
