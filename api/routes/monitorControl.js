const express = require("express");
const router = express.Router();

const checkAuth = require("../middleware/check-auth");

const monitorControl = require("../controllers/monitorControl.js");

router.post("/login", monitorControl.monitor_login);
router.post("/create", checkAuth, monitorControl.monitor_create);

router.post("/group/create", checkAuth, monitorControl.group_create);

router.get("/", checkAuth, monitorControl.get_groups);

router.ws("/register", monitorControl.register);
module.exports = router;
