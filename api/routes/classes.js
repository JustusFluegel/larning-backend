const express = require("express");
const router = express.Router();

const ClassesController = require("../controllers/classes");
const checkAuth = require("../middleware/check-auth");

router.post("/", checkAuth, ClassesController.create_class);

module.exports = router;
