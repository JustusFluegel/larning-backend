const express = require("express");
const router = express.Router();
const multer = require("multer");

const checkAuth = require("../middleware/check-auth");

let filename = "";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "upload/substitutionSchedules/");
  },
  filename: (req, file, cb) => {
    filename = new Date().getTime() + file.originalname;
    cb(null, filename);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype == "application/gzip") {
    cb(null, true);
  } else {
    const error = new Error(
      "Unsupported file type: '" +
        file.mimetype +
        "'. Only application/gzip is supported."
    );
    error.status = 404;
    cb(error, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10
  },
  fileFilter: fileFilter
});

const substitutionSchedulesConntroller = require("../controllers/substitutionSchedule");

router.post(
  "/untis",
  upload.single("tarArchive"),
  (req, res, next) => {
    req.filename = filename;
    req.filepath = __dirname + "/../../upload/substitutionSchedules/";
    next();
  },
  checkAuth,
  substitutionSchedulesConntroller.parseSubstitutionScheduleFiles
);

router.get(
  "/monitor/:timestamp",
  checkAuth,
  substitutionSchedulesConntroller.getSustitutionScheduleForMonitor
);

router.get(
  "/:timestamp",
  checkAuth,
  substitutionSchedulesConntroller.getSustitutionScheduleForDay
);

router.ws("/register", substitutionSchedulesConntroller.register);

module.exports = router;
