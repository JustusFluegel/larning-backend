const express = require("express");
const router = express.Router();

const UserController = require("../controllers/users");
const checkAuth = require("../middleware/check-auth");

router.post("/signup", UserController.user_signup);
router.post("/login", UserController.user_login);
router.delete("/:userId", checkAuth, UserController.user_delete);
router.put("/admin/:userId", checkAuth, UserController.user_add_admin);
router.delete("/admin/:userId", checkAuth, UserController.user_delete_admin);
router.get("/", checkAuth, UserController.user_get_all);

module.exports = router;
