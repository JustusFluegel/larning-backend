const mongoose = require("mongoose");

const classesSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  students: [{ type: mongoose.Schema.Types.ObjectId, ref: "Student" }],
  letter: { type: String },
  grade: { type: String, required: true },
  room: { type: String }
  // timetable: {type: mongoose.Schema.Types.ObjectId, ref: "Timetable"}
});

module.exports = mongoose.model("Class", classesSchema);
