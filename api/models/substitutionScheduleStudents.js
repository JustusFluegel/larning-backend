const mongoose = require("mongoose");

const vStudentsSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  hour: String,
  teacher: String,
  vteacher: String,
  lesson: String,
  class: String,
  vroom: String,
  room: String,
  vtext: String,
  timestamp: String
});

module.exports = mongoose.model(
  "SubstitutionScheduleStudents",
  vStudentsSchema
);
