const mongoose = require("mongoose");

const monitorControlGroupSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  alias: String,
  settings: {
    tables: {
        teachers: {
          order: {type: [String], default:[
            "vteacher",
            "hour",
            "vroom",
            "class",
            "lesson",
            "teacher",
            "room",
            "vtext"
          ]},
          bold: {type: [Boolean], default:[false, false, false, false, false, false, false, false]}
        },
        students: {
          order:{type: [String], default:[
            "class",
            "hour",
            "lesson",
            "teacher",
            "vteacher",
            "room",
            "vroom",
            "vtext"
          ]},
          bold:{ type: [Boolean], default:[false, false, false, false, false, false, false, false]}
        }
      },
    partition:{
      left: {type: String, default: "55%"},
      top: {type: String, default: "50%"}
    },
    headers:{
      format:{
        right:{
          weekday: {type: String, default: "long"},
          year: {type: String, default: "numeric"},
          month: {type: String, default: "long"},
          day: {type: String, default: "numeric"}
        },
        left:{
          weekday: {type: String, default: "long"},
          year: {type: String, default: "numeric"},
          month: {type: String, default: "long"},
          day: {type: String, default: "numeric"}
        }
      },
      language: {type: String, default: "de-DE"}
    }
  }
});

module.exports = mongoose.model(
  "MonitorControlGroup",
  monitorControlGroupSchema
);
