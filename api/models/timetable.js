const mongoose = require("mongoose");

const timetableSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  monday: [{ type: mongoose.Schema.Types.ObjectId, ref: "Group" }],
  tuesday: [{ type: mongoose.Schema.Types.ObjectId, ref: "Group" }],
  wednesday: [{ type: mongoose.Schema.Types.ObjectId, ref: "Group" }],
  thursday: [{ type: mongoose.Schema.Types.ObjectId, ref: "Group" }],
  friday: [{ type: mongoose.Schema.Types.ObjectId, ref: "Group" }]
});

module.exports = mongoose.model("Timetable", timetableSchema);
