const mongoose = require("mongoose");

const radiusSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  activated: { type: Boolean, default: true },
  deactivationMessage: String,
  radiusUsername: {
    type: String,
    required: true
  },
  radiusPassword: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("RadiusUser", radiusSchema);
