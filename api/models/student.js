const mongoose = require("mongoose");

const studentsModel = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  class: { type: mongoose.Schema.Types.ObjectId, ref: "Class" }
});

module.exports = mongoose.model("Student", studentsModel);
