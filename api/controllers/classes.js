const mongoose = require("mongoose");

const User = require("../models/users");
const Class = require("../models/classes");

const log4js = require("log4js");
log4js.configure({
  appenders: {
    parsefile: { type: "file", filename: "../../logs/parse.log" },
    logfile: { type: "file", filename: "../../logs/general.log" }
  },
  categories: {
    default: { appenders: ["logfile"], level: "debug" },
    parseFiles: { appenders: ["parsefile"], level: "debug" }
  }
});

const generalLogger = log4js.getLogger("default");
generalLogger.level = "error";

exports.create_class = (req, res, next) => {
  Class.findOne({
    room: req.body.room,
    vintatge: req.body.vintage
  })
    .exec()
    .then(result => {
      generalLogger.debug(JSON.stringify(result, null, "\t"));
      if (result != null) {
        res.status(400).json({
          message: "CLASS_EXISTS",
          hint: "Class was already created."
        });
      } else {
        if (
          req.permissions.includes("dev") ||
          req.permissions.includes("admin")
        ) {
          const created_class = new Class({
            _id: new mongoose.Types.ObjectId(),
            room: req.body.room,
            letter: req.body.letter,
            vintage: req.body.vintage
          });
          created_class
            .save()
            .then(result => {
              res.status(201).json({
                message: "CLASS_CREATED",
                class: {
                  _id: created_class._id,
                  room: created_class.room,
                  vintage: created_class.vintage,
                  letter: created_class.letter
                }
              });
            })
            .catch(err => {
              return res.status(400).json({
                message: "CLASS_CREATION_FAILED",
                hint: "Maybe wrong data delivered?",
                body: {
                  room: "String",
                  className: "String"
                },
                err: err
              });
            });
        } else {
          res.header(
            "WWW-Authenticate",
            'Basic realm="Create class", charset="UTF-8"'
          );
          return res.status(401).json({
            message: "INSUFFICENT_PERMISSIONS",
            permissions_needed: "dev||admin"
          });
        }
      }
    })
    .catch(err => {
      res.status(400).json({
        message: "CLASS_EXISTS",
        hint: "Class was already created.",
        err: err
      });
    });
};

exports.delete_class = (req, res, next) => {
  User.findOneAndDelete({ _id: { $eq: req.params.classId } })
    .exec()
    .then()
    .catch();
};
