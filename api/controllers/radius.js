const nthash = require("smbhash").nthash;
const mysql = require("mysql");
const connection = mysql.createConnection({
  host: process.env.RAD_MYSQL_HOST || "localhost",
  user: process.env.RAD_MYSQL_USER || "radius",
  password: process.env.RAD_MYSQL_PASS || "radiuspwd",
  database: process.env.RAD_MYSQL_DB || "radius"
});

const RadiusUser = require("../models/radius");
const mongoose = require("mongoose");
const { uniqueNamesGenerator } = require("unique-names-generator");
const generatePassword = require("password-generator");

exports.radius_get_credentials = (req, res, next) => {
  if (
    req.permissions.includes("dev") ||
    req.permissions.includes("admin") ||
    req.permissions.includes("teacher")
  ) {
    RadiusUser.find({ userId: req.userData._id })
      .populate("userId")
      .exec()
      .then(result => {
        if (result.length < 1) {
          const radiusUsername = uniqueNamesGenerator();
          const radiusPassword = generatePassword(14, true);

          const radiusUser = new RadiusUser({
            _id: new mongoose.Types.ObjectId(),
            userId: req.userData._id,
            activated: true,
            radiusUsername: radiusUsername,
            radiusPassword: radiusPassword
          });
          radiusUser
            .save()
            .then(result => {
              connection.connect();
              connection.query(
                "insert into radusergroup (username,groupname,priority) values ('" +
                  radiusUsername +
                  "','teachers','1');",
                (err, results, fields) => {
                  if (err) {
                    connection.end();
                    return res
                      .status(500)
                      .json({ message: "INTERNAL_SERVER_ERROR", err: err });
                  }
                  connection.query(
                    "insert into radcheck (username,attribute,op,value) values ('" +
                      radiusUsername +
                      "','NT-Password', ':=', '" +
                      nthash(radiusPassword) +
                      "');",
                    (err, results, fields) => {
                      connection.end();
                      if (err)
                        return res.status(500).json({
                          message: "INTERNAL_SERVER_ERROR",
                          err: err
                        });
                      return res.status(201).json({
                        message: "CREATED_RADIUS_USER",
                        radius: {
                          _id: radiusUser._id,
                          activated: radiusUser.activated,
                          deactivationMessage: radiusUser.deactivationMessage,
                          radiusUsername: radiusUser.radiusUsername,
                          radiusPassword: radiusUser.radiusPassword
                        }
                      });
                    }
                  );
                }
              );
            })
            .catch(err =>
              res
                .status(500)
                .json({ message: "INTERNAL_SERVER_ERROR", err: err })
            );
        } else {
          res.status(200).json({
            message: "GOT_RADIUS_USER",
            radius: {
              _id: result[0]._id,
              activated: result[0].activated,
              deactivationMessage: result[0].deactivationMessage,
              radiusUsername: result[0].radiusUsername,
              radiusPassword: result[0].radiusPassword
            }
          });
        }
      })
      .catch(err => {
        throw err;
        res.status(500).json({ message: "INTERNAL_SERVER_ERROR", err: err });
      });
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Upload untis file", charset="UTF-8"'
    );
    return res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "dev||admin||teacher"
    });
  }
};
