const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");

const User = require("../models/users");

const log4js = require("log4js");
log4js.configure({
  appenders: {
    parsefile: { type: "file", filename: "../../logs/parse.log" },
    logfile: { type: "file", filename: "../../logs/general.log" }
  },
  categories: {
    default: { appenders: ["logfile"], level: "debug" },
    parseFiles: { appenders: ["parsefile"], level: "debug" }
  }
});

const generalLogger = log4js.getLogger("default");
generalLogger.level = "error";

exports.user_login = (req, res, next) => {
  if (
    (req.body.email == null && req.body.username == null) ||
    req.body.password == null
  )
    return res.status(400).json({
      message: "ERR_INVALID_ARGUMENTS",
      err: "Parameters password and email or password and username required."
    });
  if (req.body.email != null) {
    User.find({ email: req.body.email })
      .exec()
      .then(user => {
        if (user.length < 1) {
          res.header(
            "WWW-Authenticate",
            'Basic realm="Log in/Get token for igplaner applications", charset="UTF-8"'
          );
          return res.status(401).json({
            message: "AUTH_FAILED"
          });
        }
        bcrypt.compare(req.body.password, user[0].password, (err, response) => {
          if (err) {
            res.header(
              "WWW-Authenticate",
              'Basic realm="Log in/Get token for igplaner applications", charset="UTF-8"'
            );
            return res.status(401).json({
              message: "AUTH_FAILED"
            });
          }
          if (response) {
            const token = jsonwebtoken.sign(
              {
                email: user[0].email,
                _id: user[0]._id,
                permissions: user[0].permissions
              },
              process.env.JSON_WEB_TOKEN_KEY,
              { expiresIn: 3700 }
            );
            return res.status(200).json({
              message: "AUTH_SUCCESS",
              token: token,
              expiresIn: 3600,
              user: {
                _id: user[0]._id,
                email: user[0].email,
                username: user[0].username,
                permissions: user[0].permissions
              },
              requests: [
                {
                  type: "DELETE",
                  description: "USER_DELETE",
                  url: "http://" + req.headers.host + "/users/:userId",
                  authentication_needed: true,
                  permissions_needed: "admin||dev||own"
                }
              ]
            });
          }

          res.header(
            "WWW-Authenticate",
            'Basic realm="Log in/Get token for igplaner applications", charset="UTF-8"'
          );
          return res.status(401).json({
            message: "AUTH_FAILED"
          });
        });
      })
      .catch();
  } else {
    User.find({ username: req.body.username })
      .exec()
      .then(user => {
        if (user.length < 1) {
          res.header(
            "WWW-Authenticate",
            'Basic realm="Log in/Get token for igplaner applications", charset="UTF-8"'
          );
          return res.status(401).json({
            message: "AUTH_FAILED"
          });
        } else {
          bcrypt.compare(
            req.body.password,
            user[0].password,
            (err, response) => {
              if (err) {
                res.header(
                  "WWW-Authenticate",
                  'Basic realm="Log in/Get token for igplaner applications", charset="UTF-8"'
                );
                return res.status(401).json({
                  message: "AUTH_FAILED"
                });
              }
              if (response) {
                const token = jsonwebtoken.sign(
                  {
                    email: user[0].email,
                    _id: user[0]._id,
                    permissions: user[0].permissions
                  },
                  process.env.JSON_WEB_TOKEN_KEY,
                  { expiresIn: 3700 }
                );
                return res.status(200).json({
                  message: "AUTH_SUCCESS",
                  token: token,
                  expiresIn: 3600,
                  user: {
                    _id: user[0]._id,
                    email: user[0].email,
                    username: user[0].username,
                    permissions: user[0].permissions
                  },
                  requests: [
                    {
                      type: "DELETE",
                      description: "USER_DELETE",
                      url: "http://" + req.headers.host + "/users/:userId",
                      authentication_needed: true,
                      permissions_needed: "admin||dev||own"
                    }
                  ]
                });
              }

              res.header(
                "WWW-Authenticate",
                'Basic realm="Log in/Get token for igplaner applications", charset="UTF-8"'
              );
              return res.status(401).json({
                message: "AUTH_FAILED"
              });
            }
          );
        }
      });
  }
};

exports.user_signup = (req, res, next) => {
  //TODO: add check for unique username @critical
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length > 0) {
        res.status(409).json({
          message: "EMAIL_EXISTS"
        });
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              message: "ERROR_HASHING",
              error: err
            });
          } else {
            const created_user = new User({
              _id: new mongoose.Types.ObjectId(),
              password: hash,
              email: req.body.email,
              username: req.body.username
            });
            created_user
              .save()
              .then(result => {
                return res.status(201).json({
                  message: "USER_CREATED",
                  user: {
                    _id: created_user._id,
                    email: created_user.email,
                    username: created_user.username,
                    permissions: created_user.permissions
                  },
                  token: jsonwebtoken.sign(
                    {
                      email: created_user.email,
                      _id: created_user._id,
                      username: created_user.username,
                      permissions: created_user.permissions
                    },
                    process.env.JSON_WEB_TOKEN_KEY,
                    { expiresIn: 3700 }
                  ),
                  expiresIn: 3600,
                  requests: [
                    {
                      type: "POST",
                      description: "USER_LOGIN",
                      url: "http://" + req.headers.host + "/users/login",
                      body: { email: "String", password: "String" },
                      authentication_needed: false,
                      permissions_needed: null
                    }
                  ]
                });
              })
              .catch(err => {
                res.status(400).json({
                  message: "DATA_VALIDATION_FAILED",
                  hint: "Maybe a invalid email adress?"
                });
              });
          }
        });
      }
    })
    .catch(err => {
      return res.status(409).json({
        message: "EMAIL_EXISTS"
      });
    });
};

exports.user_delete = (req, res, next) => {
  if (
    req.userData._id == req.params.userId ||
    req.userData.permissions.includes("dev") == true
  ) {
    User.deleteOne({ _id: req.params.userId })
      .exec()
      .then(result => {
        return res.status(200).json({
          message: "USER_DELETED",
          requests: [
            {
              type: "POST",
              description: "USER_CREATE",
              url: "http://" + req.headers.host + "/users/signup",
              body: { email: "String", password: "String" },
              authentication_needed: false,
              permissions_needed: null
            }
          ]
        });
      })
      .catch(err => {});
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Delete user", charset="UTF-8"'
    );
    res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "dev||self"
    });
  }
};

exports.user_add_admin = (req, res, next) => {
  if (req.permissions.includes("dev") == true) {
    User.findOneAndUpdate(
      {
        $and: [
          { _id: { $eq: req.params.userId } },
          { permissions: { $nin: ["admin"] } }
        ]
      },
      { $push: { permissions: "admin" } }
    )
      .exec()
      .then(user => {
        if (user == null) {
          res.status(400).json({
            message: "USER_NOT_FOUND"
          });
        } else {
          res.status(200).json({
            message: "ADMIN_ADDED",
            requests: [
              {
                type: "POST",
                description: "USER_LOGIN",
                url: "http://" + req.headers.host + "/users/login",
                body: { email: "String", password: "String" },
                authentication_needed: false,
                permissions_needed: null
              },
              {
                type: "GET",
                description: "GET_USERS_LIST",
                url: "http://" + req.headers.host + "/users/login",
                authentication_needed: true,
                permissions_needed: "dev||admin"
              }
            ]
          });
        }
      })
      .catch(err => {
        res.status(500).json({
          message: "INTERNAL_SERVER_ERROR",
          err: err
        });
      });
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Add admin permission to other users", charset="UTF-8"'
    );
    res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "dev"
    });
  }
};

exports.user_delete_admin = (req, res, next) => {
  if (req.permissions.includes("dev") == true) {
    User.findOneAndUpdate(
      {
        $and: [
          { _id: { $eq: req.params.userId } },
          { permissions: { $in: ["admin"] } }
        ]
      },
      { $pull: { permissions: { $in: ["admin"] } } }
    )
      .exec()
      .then(user => {
        if (user == null) {
          res.status(400).json({
            message: "USER_NOT_FOUND"
          });
        } else {
          res.status(200).json({
            message: "ADMIN_DELETED",
            requests: [
              {
                type: "POST",
                description: "USER_LOGIN",
                url: "http://" + req.headers.host + "/users/login",
                body: { email: "String", password: "String" },
                authentication_needed: false,
                permissions_needed: null
              },
              {
                type: "GET",
                description: "GET_USERS_LIST",
                url: "http://" + req.headers.host + "/users/login",
                authentication_needed: true,
                permissions_needed: "dev||admin"
              }
            ]
          });
        }
      })
      .catch(err => {
        res.status(500).json({
          message: "INTERNAL_SERVER_ERROR",
          err: err
        });
      });
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Add admin permission to other users", charset="UTF-8"'
    );
    res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "dev"
    });
  }
};

exports.user_get_all = (req, res, next) => {
  if (req.permissions.includes("dev") || req.permissions.includes("admin")) {
    User.find()
      .select("_id email permissions username")
      .exec()
      .then(users => {
        var filteredUsers = [];

        try {
          users.forEach(user => {
            if (user._id != req.userData._id) {
              generalLogger.debug(JSON.stringify(user, null, "\t"));
              filteredUsers.push(user);
            }
          });
        } catch (err) {
          generalLogger.error(JSON.stringify(err, null, "\t"));
        }

        return res.status(200).json({
          message: "SUCCESSFUL_GOT_ALL_USERS",
          userCount: users.length,
          users: filteredUsers,
          requests: [
            {
              type: "POST",
              description: "USER_LOGIN",
              url: "http://" + req.headers.host + "/users/login",
              body: { email: "String", password: "String" },
              authentication_needed: false,
              permissions_needed: null
            },
            {
              type: "POST",
              description: "USER_SIGNUP",
              url: "http://" + req.headers.host + "/users/signup",
              body: { email: "String", password: "String" },
              authentication_needed: false,
              permissions_needed: null
            },
            {
              type: "PUT",
              description: "USER_ADD_ADMIN",
              url: "http://" + req.headers.host + "/users/admin/:userId",
              body: { _id: "ObjectId" },
              authentication_needed: true,
              permissions_needed: "dev"
            },
            {
              type: "DELETE",
              description: "USER_DELETE",
              url: "http://" + req.headers.host + "/users/:userId",
              authentication_needed: true,
              permissions_needed: "admin||dev||own"
            }
          ]
        });
      })
      .catch(err => {
        return res.status(500).json({
          message: "INTERNAL_SERVER_ERROR",
          error: err
        });
      });
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Get list of users", charset="UTF-8"'
    );
    return res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "dev||admin"
    });
  }
};
