const mongoose = require("mongoose");
const jsonwebtoken = require("jsonwebtoken");

const MonitorControl = require("../models/monitorControl");
const MonitorControlGroup = require("../models/monitorControlGroup");

exports.monitor_create = (req, res, next) => {
  if (req.userData.permissions.includes("admin")) {
    const group = mongoose.Types.ObjectId(req.body.group);

    MonitorControlGroup.find({ _id: req.body.group })
      .exec()
      .then(response => {
        if (response.length < 1) {
          res.status(400).json({ message: "GROUP_NOT_EXIST" });
        } else {
          const _id = new mongoose.Types.ObjectId();

          const created_monitor = new MonitorControl({
            _id: _id,
            emailAlias: `${_id}@${_id}.${_id}`,
            alias: req.body.alias,
            position: req.body.position,
            group: group
          });

          created_monitor
            .save()
            .then(cr_mon => {
              MonitorControl.findOne({ _id: cr_mon._id })
                .populate("group")
                .then(monitor => {
                  return res.status(201).json({
                    message: "MONITOR_CREATED",
                    user: {
                      _id: _id,
                      monitor: {
                        email: created_monitor.emailAlias,
                        alias: created_monitor.alias,
                        position: created_monitor.position,
                        group: monitor.group,
                        permissions: ["monitor"]
                      }
                    },
                    token: jsonwebtoken.sign(
                      {
                        email: created_monitor.emailAlias,
                        _id: _id,
                        username: created_monitor.alias,
                        permissions: ["monitor"]
                      },
                      process.env.JSON_WEB_TOKEN_KEY,
                      { expiresIn: 3700 }
                    ),
                    expiresIn: 3600,
                    requests: [
                      {
                        type: "POST",
                        description: "MONITOR_LOGIN",
                        url:
                          "http://" +
                          req.headers.host +
                          "/monitorControl/login",
                        body: { _id: "ObjectId" },
                        authentication_needed: false,
                        permissions_needed: null
                      }
                    ]
                  });
                })
                .catch(err => {});
            })
            .catch(err => {
              res.status(500).json({
                message: "DATA_VALIDATION_FAILED",
                hint: "Some strange is happening.",
                err: err
              });
            });
        }
      })
      .catch(err => {});
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Delete user", charset="UTF-8"'
    );
    res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "admin"
    });
  }
};

exports.monitor_login = (req, res, next) => {
  MonitorControl.find({ _id: req.body._id })
    .populate("group")
    .exec()
    .then(monitor => {
      if (monitor.length < 1) {
        res.header(
          "WWW-Authenticate",
          'Basic realm="Log in/Get token for igplaner applications", charset="UTF-8"'
        );
        return res.status(401).json({
          message: "AUTH_FAILED"
        });
      } else {
        const token = jsonwebtoken.sign(
          {
            email: monitor[0].emailAlias,
            _id: monitor[0]._id,
            username: monitor[0].alias,
            permissions: ["monitor"]
          },
          process.env.JSON_WEB_TOKEN_KEY,
          { expiresIn: 3700 }
        );
        return res.status(200).json({
          message: "AUTH_SUCCESS",
          token: token,
          expiresIn: 3600,
          monitor: {
            _id: monitor[0]._id,
            alias: monitor[0].alias,
            position: monitor[0].position,
            group: monitor[0].group,
            email: monitor[0].emailAlias,
            permissions: ["monitor"]
          }
        });
      }
    })
    .catch(err => {
      res.status(500).json({ err: err });
    });
};

exports.group_create = (req, res, next) => {
  if (req.userData.permissions.includes("admin")) {
    const created_group = new MonitorControlGroup({
      _id: new mongoose.Types.ObjectId(),
      alias: req.body.alias
    });
    created_group
      .save()
      .then(response => {
        return res.status(201).json({
          message: "GROUP_CREATED",
          group: {
            _id: created_group._id,
            alias: created_group.alias
          },
          requests: [
            {
              type: "POST",
              description: "CREATE_MONITOR",
              url: "http://" + req.headers.host + "/monitorControl/create",
              body: { alias: "String", position: "String", group: "ObjectId" },
              authentication_needed: true,
              permissions_needed: ["admin"]
            }
          ]
        });
      })
      .catch(err => {
        res.status(500).json({
          message: "DATA_VALIDATION_FAILED",
          hint: "Some strange is happening."
        });
      });
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Delete user", charset="UTF-8"'
    );
    res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "admin"
    });
  }
};

const listeners = [];

exports.register = (ws, req) => {
  console.log("Cregister");
  const listenerId = listeners.length;
  listeners.push(msg => {
    try {
      ws.send(msg);
    } catch (err) {
      listeners[listenerId] = () => {};
    }
  });
  ws.send("REGISTERED");
};

function notThatKeys(obj, keys) {
  const newObj = {};
  Object.keys(obj)
    .filter(key => {
      let valid = true;
      keys.forEach(notAcceptableKey => {
        if (valid && key == notAcceptableKey) valid = false;
      });
      return valid;
    })
    .forEach(key => {
      newObj[key] = obj[key];
    });
  return newObj;
}

exports.get_groups = async (req, res, next) => {
  if (req.userData.permissions.includes("admin")) {
    try {
      const monitors = await MonitorControl.find({}).exec();
      const groups = await MonitorControlGroup.find({}).exec();

      const groupsId = {};
      groups.forEach(group => {
        groupsId[group._id] = notThatKeys(group._doc, ["_id", "__v"]);
      });

      const monitorId = {};
      monitors.forEach(monitor => {
        monitorId[monitor._id] = notThatKeys(monitor._doc, ["_id", "__v"]);
      });

      Object.keys(groupsId).forEach(key => {
        groupsId[key].monitors = {};
        Object.keys(monitorId)
          .filter(monKey => monitorId[monKey].group == key)
          .forEach(monKey => {
            groupsId[key].monitors[monKey] = monitorId[monKey];
          });
      });

      res.status(200).json({
        message: "GOT_GROUPS_SUCCESSFUL",
        groups: groupsId
      });
    } catch (err) {
      res.status(500).json({
        message: "ERR_DATABSE",
        hint: "Error whilest fetching data from Database",
        err: err
      });
    }
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Delete user", charset="UTF-8"'
    );
    res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "admin"
    });
  }
};
