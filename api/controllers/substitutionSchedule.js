const fs = require("fs");
const mongoose = require("mongoose");
const untis_mon_to_json = require("untis-mon-to-json");

const substitutionScheduleStudentsModel = require("../models/substitutionScheduleStudents");
const substitutionScheduleTeachersModel = require("../models/substitutionScheduleTeachers");

const log4js = require("log4js");
log4js.configure({
  appenders: {
    parsefile: { type: "file", filename: "../logs/parse.log" },
    logfile: { type: "file", filename: "../logs/general.log" }
  },
  categories: {
    default: { appenders: ["logfile"], level: "debug" },
    parseFiles: { appenders: ["parsefile"], level: "debug" }
  }
});

const parseLogger = log4js.getLogger("parseFiles");
parseLogger.level = "error";

const listeners = [];

exports.parseSubstitutionScheduleFiles = (req, res, next) =>
  (async () => {
    if (
      req.permissions.includes("dev") ||
      req.permissions.includes("untis") ||
      req.permissions.includes("admin")
    ) {
      const fpath = req.filepath + req.filename;

      const {
        students,
        teachers
      } = await untis_mon_to_json.readUntisTarGzArchive(fpath);

      if (students != null) {
        const studentTimestamps = students.map(student => student.timestamp);
        const parsedStudents = students.map(student => ({
          ...student,
          _id: new mongoose.Types.ObjectId()
        }));

        studentTimestamps.forEach(studentTimestamp => {
          substitutionScheduleStudentsModel.deleteMany(
            {
              timestamp: { $eq: studentTimestamp }
            },
            err => {
              parseLogger.error(
                "An error occured while deleting old student entries: \n" +
                  JSON.stringify(err, null, "\t")
              );
            }
          );
        });

        substitutionScheduleStudentsModel.create(
          parsedStudents,
          (err, result) => {
            if (err) throw err;
          }
        );
      }
      if (teachers != null) {
        const teacherTimestamps = teachers.map(student => student.timestamp);
        const parsedTeachers = teachers.map(student => ({
          ...student,
          _id: new mongoose.Types.ObjectId()
        }));

        teacherTimestamps.forEach(teacherTimestamp => {
          substitutionScheduleTeachersModel.deleteMany(
            {
              timestamp: { $eq: teacherTimestamp }
            },
            err => {
              parseLogger.error(
                "An error occured while deleting old teacher entries: \n" +
                  JSON.stringify(err, null, "\t")
              );
            }
          );
        });

        substitutionScheduleTeachersModel.create(
          parsedTeachers,
          (err, result) => {
            if (err) throw err;
          }
        );
      }
      fs.unlinkSync(fpath);
      listeners.forEach(send => {
        send("NEW_DATA");
      });
      res.status(200).json({
        message: "UPLOADED_FILE"
      });
    } else {
      res.header(
        "WWW-Authenticate",
        'Basic realm="Upload untis file", charset="UTF-8"'
      );
      return res.status(401).json({
        message: "INSUFFICENT_PERMISSIONS",
        permissions_needed: "dev||untis"
      });
    }
  })();

// exports.parseSubstitutionScheduleFiles = (req, res, next) => {
//   if (req.permissions.includes("dev") || req.permissions.includes("untis")) {
//     const fpath = req.filepath + req.filename;

//     untis_mon_to_json.readUntisTarGzArchive(fpath, function(
//       students,
//       teachers
//     ) {
//       if (students != null) {
//         var studentTimestamps = [];

//         var parsedStudents = [];
//         var parsedTeachers = [];

//         students.forEach(student => {
//           if (!studentTimestamps.includes(student["timestamp"])) {
//             studentTimestamps.push(student["timestamp"]);
//           }
//           parsedStudents.push({
//             ...student,
//             _id: new mongoose.Types.ObjectId()
//           });
//         });

//         var teacherTimestamps = [];

//         teachers.forEach(teacher => {
//           if (!teacherTimestamps.includes(teacher["timestamp"])) {
//             teacherTimestamps.push(teacher["timestamp"]);
//           }
//           parsedTeachers.push({
//             ...teacher,
//             _id: new mongoose.Types.ObjectId()
//           });
//         });

//         studentTimestamps.forEach(studentTimestamp => {
//           substitutionScheduleStudentsModel.deleteMany(
//             {
//               timestamp: { $eq: studentTimestamp }
//             },
//             err => {
//               parseLogger.error(
//                 "An error occured while deleting old student entries: \n" +
//                   JSON.stringify(err, null, "\t")
//               );
//             }
//           );
//         });

//         teacherTimestamps.forEach(teacherTimestamp => {
//           substitutionScheduleTeachersModel.deleteMany(
//             {
//               timestamp: { $eq: teacherTimestamp }
//             },
//             err => {
//               parseLogger.error(
//                 "An error occured while deleting old teacher entries: \n" +
//                   JSON.stringify(err, null, "\t")
//               );
//             }
//           );
//         });

//         substitutionScheduleStudentsModel.create(
//           parsedStudents,
//           (err, result) => {
//             if (err) throw err;
//           }
//         );

//         substitutionScheduleTeachersModel.create(
//           parsedTeachers,
//           (err, result) => {
//             if (err) throw err;
//           }
//         );
//         fs.unlinkSync(fpath);
//         listeners.forEach(send => {
//           send("NEW_DATA");
//         });
//         res.status(200).json({
//           message: "UPLOADED_FILE"
//         });
//       } else {
//         res.status(500).json({
//           message: "INTERNAL_SERVER_ERROR",
//           err: teachers
//         });
//       }
//     });
//   } else {
//     res.header(
//       "WWW-Authenticate",
//       'Basic realm="Upload untis file", charset="UTF-8"'
//     );
//     return res.status(401).json({
//       message: "INSUFFICENT_PERMISSIONS",
//       permissions_needed: "dev||untis"
//     });
//   }
// };

exports.getSustitutionScheduleForDay = async (req, res, next) => {
  if (
    req.permissions.includes("dev") ||
    req.permissions.includes("untis") ||
    req.permissions.includes("monitor")
  ) {
    if (req.params.timestamp != null) {
      try {
        let students = await substitutionScheduleStudentsModel
          .find({
            timestamp: { $eq: req.params.timestamp }
          })
          .select("_id class hour teacher vteacher lesson vroom room vtext")
          .exec();

        let teachers = await substitutionScheduleTeachersModel
          .find()
          .select("_id class hour teacher vteacher lesson vroom room vtext")
          .exec({
            timestamp: { $eq: req.params.timestamp }
          });
        res.status(200).json({
          message: "DATA_SUCCESFULLY_RECIVED",
          students: students,
          teachers: teachers
        });
      } catch (err) {
        res.status(500).json({
          message: "ERROR_WHILEST_GETTING_DATA_FROM_DATABASE",
          err: err
        });
      }
    } else {
      res.status(400).json({
        message: "NEED_DAY",
        hint: "You must send a day for getting data."
      });
    }
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Get all vplan data for one specific day.", charset="UTF-8"'
    );
    res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "dev||untis||monitor"
    });
  }
};

exports.getSustitutionScheduleForMonitor = async (req, res, next) => {
  if (
    req.permissions.includes("dev") ||
    req.permissions.includes("untis") ||
    req.permissions.includes("monitor")
  ) {
    if (req.params.timestamp != null) {
      try {
        let days = untis_mon_to_json.nextSchoolDays(req.params.timestamp);

        let day1 = days[0];
        let day2 = days[1];

        let day1Students = await substitutionScheduleStudentsModel
          .find({
            timestamp: { $eq: day1 }
          })
          .select("_id class hour teacher vteacher lesson vroom room vtext")
          .exec();

        let day1Teachers = await substitutionScheduleTeachersModel
          .find({
            timestamp: { $eq: day1 }
          })
          .select("_id class hour teacher vteacher lesson vroom room vtext")
          .exec();

        let day2Students = await substitutionScheduleStudentsModel
          .find({
            timestamp: { $eq: day2 }
          })
          .select("_id class hour teacher vteacher lesson vroom room vtext")
          .exec();

        let day2Teachers = await substitutionScheduleTeachersModel
          .find({
            timestamp: { $eq: day2 }
          })
          .select(
            "_id class hour teacher vteacher lesson vroom room vtext timestamp"
          )
          .exec();

        res.status(200).json({
          message: "DATA_SUCCESFULLY_RECIVED",
          1: {
            description: "Data of passed day or next school day",
            students: day1Students,
            teachers: day1Teachers,
            timestamp: day1
          },
          2: {
            description:
              "Data of the school day after passed day or next school day",
            students: day2Students,
            teachers: day2Teachers,
            timestamp: day2
          }
        });
      } catch (err) {
        res.status(500).json({
          message: "ERROR_WHILEST_GETTING_DATA_FROM_DATABASE",
          err: err
        });
      }
    } else {
      res.status(400).json({
        message: "NEED_DAY",
        hint: "You must send a day for getting data."
      });
    }
  } else {
    res.header(
      "WWW-Authenticate",
      'Basic realm="Get all vplan data for monitors.", charset="UTF-8"'
    );
    res.status(401).json({
      message: "INSUFFICENT_PERMISSIONS",
      permissions_needed: "dev||untis||monitor"
    });
  }
};

exports.register = (ws, req) => {
  console.log("register");
  const listenerId = listeners.length;
  listeners.push(msg => {
    try {
      ws.send(msg);
    } catch (err) {
      listeners[listenerId] = () => {};
    }
  });
  ws.send("REGISTERED");
};
