const jsonwebtoken = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jsonwebtoken.verify(token, process.env.JSON_WEB_TOKEN_KEY);
    req.userData = decoded;
    req.permissions = decoded.permissions;
    next();
  } catch (err) {
    res.header(
      "WWW-Authenticate",
      'Bearer realm="Acess restful api for igplaner applications" charset="UTF-8"'
    );
    return res.status(401).json({
      message: "TOKEN_AUTH_FAILED"
    });
  }
};
