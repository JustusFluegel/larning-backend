---
sidebarDepth: 2
---

# IGPLaner API Documentation

## Use of this page

- This page is used as an guide how to use our API.
- This page gives you all the neccesary Information to work with our API

## Quick Start

### Login

If you have alreay an IGPlaner dev server up and running, you can create the first request to our api. Most times, if you have signed up trough our app, you can directly use an Login request.

```
TYPE: POST
BODY: JSON
RESPONSE: JSON,
PATH: https://yourserver.com/users/login
```

So to log in, you only have to create an request with the body like this:

```json
{
  "email": "Your Email",
  "password": "Your Password"
}
```

I reccomend installing [postman](https://getpostman.com/downloads) for manual api requests.

You will get back an response like this:

```json
{
  "message": "AUTH_SUCCESS",
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImtsZXR0ZWxhcnNAZ21haWwuY29tIiwiX2lkIjoiNWM4NzZjMzVmN2Y3ZmI1NWFiZDFiOWU5IiwicGVybWlzc2lvbnMiOlsiZGV2IiwiYWRtaW4iXSwiaWF0IjoxNTY2NTc3OTk1LCJleHAiOjE1NjY1ODE2OTV9.lxWe2o0beIsiBx9YqJRovvHxyghy_e1u6Oe7qTZ_yTE", // The token needed for authorization on request
  "expiresIn": 3600, // How long the login is valid
  "user": {
    "_id": "5c876c35f7f7fb55abd1b9e9", // User id needed for querries on the user
    "email": "Your Email",
    "username": "Your Username",
    "permissions": ["dev", "admin"] //Example Permissions
  },
  // Further actions on this user
  "requests": [
    {
      "type": "DELETE",
      "description": "USER_DELETE",
      "url": "http://yourserver.com:3001/users/:userId",
      "authentication_needed": true,
      "permissions_needed": "admin||dev||own"
    }
  ]
}
```

It's completly valid json, you can parse it how you want.

Congratulations! You have sucessfully logged in first time!

### Signup

If you haven't an account, you can directly signup trough the api:

```
TYPE: POST
BODY: JSON
RESPONSE: JSON
PAth: https://yourserver.com/users/signup
```

So to sign up, you only have to create an POST reqest with an body like this:

```json
{
  "email": "Your Email",
  "username": "Your Username",
  "password": "Your choosen Password"
}
```

I reccomend installing [postman](https://getpostman.com/downloads) for manual api requests.

You will get back an response like this:

```json
{
  "message": "USER_CREATED",
  "user": {
    "_id": "5d601945ad8b406c95b39599", // User id for further actions on ths user
    "email": "Your email",
    "username": "Your username",
    "permissions": []
  },
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImtsZXR0ZWxhcnN0QGdtYWlsLmNvbSIsIl9pZCI6IjVkNjAxOTQ1YWQ4YjQwNmM5NWIzOTU5OSIsInVzZXJuYW1lIjoidGVzdCIsInBlcm1pc3Npb25zIjpbXSwiaWF0IjoxNTY2NTc5MDEzLCJleHAiOjE1NjY1ODI3MTN9.bW49U12WXN5cU8e397eP9uZuVv7y5Bm_CEMu9tlFpjs", //JsonWebToken for authorization on further requests
  "expiresIn": 3600, // How long the autologin is valid
  // Further requests
  "requests": [
    {
      "type": "POST",
      "description": "USER_LOGIN",
      "url": "http://192.168.1.39:3001/users/login",
      "body": {
        "email": "String",
        "password": "String"
      },
      "authentication_needed": false,
      "permissions_needed": null
    }
  ]
}
```

It's completly valid json, you can parse it how you want.

Congratulations! You have sucessfully signed up first time!
