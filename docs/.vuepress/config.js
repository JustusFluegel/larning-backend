module.exports = {
  title: "IGPlaner API Documentation",
  description: "The documentation for the IGPlaner API",
  themeConfig: {
    sidebar: [
      ["/", "Home"],
      {
        title: "User management",
        collapsable: true,
        children: [["/user_mgmt/login", "Login"]]
      }
    ]
  }
};
