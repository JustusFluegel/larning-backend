const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const app = express();

const ws = require("express-ws")(app);

app.ws("/wsping", (ws, req) => {
  ws.send("pong");
  ws.on("message", msg => {
    ws.send("pong: " + msg);
  });
});

const userRoutes = require("./api/routes/users");
const classRoutes = require("./api/routes/classes");
const substitutionScheduleRoutes = require("./api/routes/substitutionSchedules");
const monitorControl = require("./api/routes/monitorControl");
const radiusRoutes = require("./api/routes/radius");

const log4js = require("log4js");
log4js.configure({
  appenders: {
    parsefile: { type: "file", filename: "./logs/parse.log" },
    logfile: { type: "file", filename: "./logs/general.log" }
  },
  categories: {
    default: { appenders: ["logfile"], level: "debug" },
    parseFiles: { appenders: ["parsefile"], level: "debug" }
  }
});

const generalLogger = log4js.getLogger("default");
generalLogger.level = "debug";

const uri =
  "mongodb://" +
  (process.env.MONGODB_USER || "igplaner") +
  ":" +
  (process.env.MONGODB_PASSWORD || "igplaner") +
  "@" +
  (process.env.MONGODB_HOST || "localhost") +
  ":" +
  (process.env.MONGODB_PORT || "27017") +
  "/" +
  (process.env.MONGODB_DB || process.env.MONGODB_USER || "igplaner");

mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true
  })
  .then(result => {
    generalLogger.debug("Connected to db...");
    generalLogger.debug("Waiting for requests...");
  })
  .catch(err => {
    generalLogger.error(JSON.stringify(err, null, "\t"));
    generalLogger.error(JSON.stringify(uri, null, "\t"));
  });
app.set("x-powered-by", false);

app.use(morgan("dev", { skip: (req, res) => req.originalUrl == "/ping" }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    req.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    res.status(200).json({});
  }
  next();
});

app.use("/users", userRoutes);
app.use("/classes", classRoutes);
app.use("/substitutionSchedule", substitutionScheduleRoutes);
app.use("/monitorControl", monitorControl);
app.use("/radius", radiusRoutes);

app.use("/ping", (req, res, next) => {
  res.status(200).send("PONG");
});

app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
