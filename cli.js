#!/usr/bin/env node

var exec = require("child_process").exec;

const [, , ...args] = process.argv;
const fs = require("fs");

const pid = fs.readFileSync(__dirname + "/pid.txt").toString();

switch (args[0]) {
  case "start":
    if (pid.includes("NOT_RUNNING")) {
      exec(
        "cd " +
          __dirname +
          "/../..; npm explore igplaner-back-dev -- npm run start &>/dev/null &"
      );
      console.log("Started.");
      process.exit(0);
    } else {
      console.log("Already running.");
    }
    return;
  case "auto-start":
    if (pid.includes("NOT_RUNNING")) {
      if (args[1] != null) {
        exec(
          "cd " +
            __dirname +
            "/../..; npm explore igplaner-back-dev -- npm run start"
        );
      } else {
        exec(
          "cd " +
            __dirname +
            "/../..; npm explore igplaner-back-dev -- npm run start"
        );
      }
    } else {
      console.log("Already running.");
      process.exit(1);
    }
    break;
  case "stop":
    if (!pid.includes("NOT_RUNNING")) {
      const pid = fs.readFileSync(__dirname + "/pid.txt");
      fs.writeFileSync(__dirname + "/pid.txt", "NOT_RUNNING");
      process.kill(pid, "SIGTERM");
      console.log("Stopped.");
    } else {
      console.log("Not running.");
    }
    return;
  case "reset":
    fs.writeFileSync(__dirname + "/pid.txt", "NOT_RUNNING");
    break;
}

process.on("SIGTERM", () => {
  const pid = fs.readFileSync(__dirname + "/pid.txt");
  if (!pid.toString().includes("NOT_RUNNING")) {
    fs.writeFileSync(__dirname + "/pid.txt", "NOT_RUNNING");
    process.kill(pid, "SIGTERM");
    console.log("Stopped.");
  }
});

process.on("SIGINT", () => {
  const pid = fs.readFileSync(__dirname + "/pid.txt");
  if (!pid.toString().includes("NOT_RUNNING")) {
    fs.writeFileSync(__dirname + "/pid.txt", "NOT_RUNNING");
    process.kill(pid, "SIGTERM");
    console.log("Stopped.");
  }
});

process.on("SIGTSTP", () => {
  const pid = fs.readFileSync(__dirname + "/pid.txt");
  if (!pid.toString().includes("NOT_RUNNING")) {
    fs.writeFileSync(__dirname + "/pid.txt", "NOT_RUNNING");
    process.kill(pid, "SIGTERM");
    console.log("Stopped.");
  }
});

process.on("SIGQUIT", () => {
  const pid = fs.readFileSync(__dirname + "/pid.txt");
  if (!pid.toString().includes("NOT_RUNNING")) {
    fs.writeFileSync(__dirname + "/pid.txt", "NOT_RUNNING");
    process.kill(pid, "SIGTERM");
    console.log("Stopped.");
  }
});
