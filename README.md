# IGPlaner Backend

IGPlaner backend is the backend for a School System called IGPlaner.

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install igplaner-back-dev.

#### Local folder

```bash
npm install --unsafe-perm igplaner-back-dev
```

#### Global

```bash
npm install -g --unsafe-perm igplaner-back-dev
```

This package requires a working mongodb installation.

### Autostart

If you are using systemd, you can use this script:

#### Local folder

```
[Unit]
Description=IGPlaner ReSTful api.
After=network.target

[Service]
ExecStart=<installationPath>/node_modules/.bin/igplaner-dev-cli auto-start

[Install]
WantedBy=multi-user.target
```

!!You need to insert your installation path!!

#### Global

```
[Unit]
Description=IGPlaner ReSTful api.
After=network.target

[Service]
ExecStart=/usr/bin/igplaner-dev-cli auto-start

[Install]
WantedBy=multi-user.target
```

In most cases the path to igplaner-dev-cli is /usr/bin/igplaner-dev-cli, if not please change it to your system.

#### Both

##### Enable autostart

in my case igplaner-back-dev is the service name, please change it to your choice.

```bash
systemctl enable igplaner-back-dev
```

##### Disable autostart

```bash
systemctl disable igplaner-back-dev
```

## Usage

The package is managed by environment variables:

```
"PORT": "<Port of the backend, default 3000>",
"MONGODB_USER": "<User of your mongodb>",
"MONGODB_PASSWORD": "<Password of the user>",
"MONGODB_HOST": "<Mongodb host, default localhost>",
"MONGODB_PORT": "<Mongodb port, default 27017>",
"MONGODB_DB": "<Mongodb database>",
"JSON_WEB_TOKEN_KEY": "<some random string for generating json web tokens>"
```

### Starting and stopping

#### Local folder

```bash
<installationPath>/node_modules/.bin/igplaner-dev-cli start     //starting
<installationPath>/node_modules/.bin/igplaner-dev-cli stop      //stopping
```

#### Global

```bash
igplaner-dev-cli start                                          //starting
igplaner-dev-cli stop                                           //stopping
```

#### With systemd

in my case igplaner-back-dev is the service name, please change it to your choice.

```bash
sudo systemctl start igplaner-back-dev                          //starting
sudo systemctl stop igplaner-back-dev                           //stopping
sudo systemctl status igplaner-back-dev                         //status
```

or

```bash
sudo service igplaner-back-dev start                            //starting
sudo service igplaner-back-dev stop                             //stopping
sudo service igplaner-back-dev status                           //status
```


## License

[CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/)

