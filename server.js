process.title = process.argv[2];

const fs = require("fs");

const pid = process.pid;
fs.writeFileSync("./pid.txt", pid);

const mongoose = require("mongoose");

const http = require("http");

const app = require("./app");

const server = app.listen(process.env.PORT || 3001);

// const websocket = require("ws");

// const wss = new websocket.Server({server});

// wss.on('connection', function connection(ws) {
//   ws.on('message', function incoming(message) {
//     ws.send(message)
//   });

//   ws.send('something');
// });

// process.on("SIGTERM", () => {
//   server.close(() => {
//     mongoose.connection.close(false, () => {
//       process.exit(0);
//     });
//   });
// });
